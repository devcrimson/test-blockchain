pragma solidity ^0.8.20;

contract SimpleSolidity {
    uint public number;

    function setNumber(uint _number) public {
        number = _number;
    }

    function getNumber() public view returns (uint) {
        return number;
    }
}
