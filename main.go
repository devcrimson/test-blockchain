package main

import (
	"blockchain/api"
	"context"
	"crypto/ecdsa"
	"fmt"
	"github.com/ethereum/go-ethereum/accounts/abi/bind"
	"github.com/ethereum/go-ethereum/crypto"
	"github.com/ethereum/go-ethereum/ethclient"
	"math/big"
)

const PrivateKey = "eda95cc5c1f238bd3f1347719f2d0bfd1d7c2f36c107ff3ebef310f1335b595f"

func main() {
	client, err := ethclient.Dial("https://eth-sepolia.g.alchemy.com/v2/YEQJKJxI7DNKPMAovgR3k1dv81zlsMnt")
	if err != nil {
		panic(err)
	}
	auth := getAccountAuth(client, PrivateKey)
	address, _, newApi, err := api.DeployApi(auth, client)
	if err != nil {
		panic(err)
	}
	fmt.Println("Address:", address)
	number, err := newApi.GetNumber(new(bind.CallOpts))
	if err != nil {
		panic(err)
	}
	fmt.Println("Got number:", number)
	_, err = newApi.SetNumber(new(bind.TransactOpts), big.NewInt(230))
	if err != nil {
		panic(err)
	}
	fmt.Println("Setting number")
	number, err = newApi.GetNumber(new(bind.CallOpts))
	if err != nil {
		panic(err)
	}
	fmt.Println("New number:", number)
}

func getAccountAuth(client *ethclient.Client, accountAddress string) *bind.TransactOpts {
	privateKey, err := crypto.HexToECDSA(accountAddress)
	if err != nil {
		panic(err)
	}
	publicKey := privateKey.Public()
	publicKeyECDSA, ok := publicKey.(*ecdsa.PublicKey)
	if !ok {
		panic("invalid key")
	}
	fromAddress := crypto.PubkeyToAddress(*publicKeyECDSA)
	nonce, err := client.PendingNonceAt(context.Background(), fromAddress)
	if err != nil {
		panic(err)
	}
	chainID, err := client.ChainID(context.Background())
	if err != nil {
		panic(err)
	}
	auth, err := bind.NewKeyedTransactorWithChainID(privateKey, chainID)
	if err != nil {
		panic(err)
	}
	auth.Nonce = big.NewInt(int64(nonce))
	auth.Value = big.NewInt(0)      // in wei
	auth.GasLimit = uint64(3000000) // in units
	auth.GasPrice = big.NewInt(1000000)
	return auth
}
